# Introduction

This is a hackish maven repo; used for hosting personal projects.

# Usage

If you're using Maven, add the following `repository` entry to your `pom.xml` file:

```
<repositories>
  <repository>
    <id>brandonwillard-mvn-repo</id>
    <snapshots>
        <enabled>true</enabled>
    </snapshots>
    <url>https://bitbucket.org/brandonwillard/mvn-repo/raw/snapshots</url>
  </repository>
</repositories>
```

# Development

If you're contributing to a project that uses this repository and want to
push an update, try something like this:

```
> mvn clean deploy
```

Of course, you'll need to be set up with proper SSH credentials and whatnot.


If you're just adding a Jar to be tracked, you'll need the following in your
`pom.xml`:

```
<distributionManagement>
  <repository>
    <id>brandonwillard-mvn-repo</id>
    <name>brandonwillard maven repository</name>
    <url>git:releases://git@bitbucket.org:brandonwillard/mvn-repo.git</url>
  </repository>
  <snapshotRepository>
    <id>brandonwillard-mvn-snapshot-repo</id>
    <name>brandonwillard maven repository snapshots</name>
    <url>git:snapshots://git@bitbucket.org:brandonwillard/mvn-repo.git</url>
  </snapshotRepository>
</distributionManagement>
```

I think I was using some `wagon` plugin, too:

```
<build>
  <extensions>
    <extension>
      <groupId>ar.com.synergian</groupId>
      <artifactId>wagon-git</artifactId>
      <version>0.2.0</version>
    </extension>
  </extensions>
</build>

<pluginRepositories>
  <pluginRepository>
    <id>synergian-repo</id>
    <url>https://raw.github.com/synergian/wagon-git/releases</url>
  </pluginRepository>
</pluginRepositories>

<repositories>  
  <repository>
    <id>wagon-git</id>
    <url>git:releases://git@github.com:synergian/wagon-git.git</url>
  </repository>
</repositories>  

```
